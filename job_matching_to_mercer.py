import pandas as pd

# matched_set_many = r'\\filer02\public\CoreyF\PythonFiles\matched_data\20180101\matched_set_many_surveys.csv'
# matched_set_many_df = pd.io.parsers.read_csv(matched_set_many, error_bad_lines=False, iterator=True, chunksize=1000)
# matched_set_many_df = pd.concat(matched_set_many_df, ignore_index=True)
# # matched_set_many_df.to_csv(r'C:\Users\ThomasM\PycharmProjects\Marketpay Investigations\text files\matched_set_many_df.csv')
# # print(matched_set_many_df.columns)
# #
# # #TODO: Use Mercer Bool in matched_set_many_df (DONE)
# #
# """Creating Mercer Bool column and Mercer only match_set_full"""
# matched_set_many_df['mercer_bool'] = matched_set_many_df['survey_job_survey_code'].str.startswith('MER-')
# matched_set_many_df['survey_job_count'] = matched_set_many_df['survey_job_title'].value_counts()
# matched_set_mercer_df = matched_set_many_df[matched_set_many_df['mercer_bool'] == True]


"""START HERE TO SAVE TIME"""

matched_set_mercer_df = r'\\filer01\DataAnalyticsCoSo\TomM\Mercer Matches\matched_set_mercer_df.csv'
matched_set_mercer_df = pd.read_csv(matched_set_mercer_df, nrows=10000)
print(matched_set_mercer_df[:10])
# print(matched_set_mercer_df.columns)
matched_set_mercer_df = matched_set_mercer_df[['survey_job_title', 'survey_job_survey_code', 'org_job_title', 'employee_title', 'ps_title']]

matched_set_mercer_df = pd.melt(matched_set_mercer_df, id_vars=['survey_job_title', 'survey_job_survey_code'], var_name='title_type', value_name='title_to_match' )

# print(matched_set_mercer_df[:10])
#TODO: Sort by counts and de duping is a good way to keep highest counts and drop others (DONE BELOW)

# matched_set_mercer_df = matched_set_mercer_df[['survey_job_title', 'survey_job_survey_code']]

groupby_org_job_title = matched_set_mercer_df.groupby(['survey_job_title', 'survey_job_survey_code', 'title_to_match'])['title_to_match'].count()
# groupby_org_job_title2 = matched_set_mercer_df.groupby(['survey_job_title', 'survey_job_survey_code', 'org_job_title'])['org_job_title'].value_counts()
# groupby_org_job_title3 = matched_set_mercer_df.groupby(['survey_job_title', 'survey_job_survey_code'])['org_job_title'].count()
# groupby_org_job_title4 = matched_set_mercer_df.groupby(['survey_job_title', 'survey_job_survey_code'])['org_job_title'].value_counts()
#TODO order by counts then de dup survey_job_title combined with _survey_code, keeping only the first (highest count) (DONE BELOW)

# groupby_org_job_title.to_csv(r'C:\Users\ThomasM\PycharmProjects\Marketpay Investigations\text files\10.11 melted_job_title_test1.csv')
# groupby_org_job_title2.to_csv(r'C:\Users\ThomasM\PycharmProjects\Marketpay Investigations\text files\10.11 org_job_value_counts_test2.csv')
# groupby_org_job_title3.to_csv(r'C:\Users\ThomasM\PycharmProjects\Marketpay Investigations\text files\10.11 org_job_value_counts_test3.csv')
# groupby_org_job_title4.to_csv(r'C:\Users\ThomasM\PycharmProjects\Marketpay Investigations\text files\10.11 org_job_value_counts_test4.csv')


#TODO make groupbys into a dataframe then sort values ascending = False (high to low) (DONE)

groupby_org_job_title_df = groupby_org_job_title.to_frame()
groupby_mercer_set_full_df = groupby_org_job_title_df.add_suffix('_count').reset_index()
print(len(groupby_mercer_set_full_df))
# groupby_mercer_set_full_df = groupby_mercer_set_full_df.sort_values(['title_to_match', 'title_to_match_count'], ascending=[True, False])
# groupby_mercer_set_full_df.to_csv(r'C:\Users\ThomasM\PycharmProjects\Marketpay Investigations\text files\10.12 melted_sort_test1.csv')
# groupby_mercer_set_full_df = groupby_mercer_set_full_df.drop_duplicates(subset=['survey_job_survey_code', 'title_to_match'])
# groupby_mercer_set_full_df.to_csv(r'C:\Users\ThomasM\PycharmProjects\Marketpay Investigations\text files\10.11 dedup_test1.csv')

#TODO: DROP DUPLICATES WITH A GROUPBY AND APPLY THAT ACCOUNTS FOR AND KEEPS TIES (DONE)
# print(groupby_mercer_set_full_df[:10])
id_max_df = groupby_mercer_set_full_df.groupby(['survey_job_survey_code', 'title_to_match'])['title_to_match_count'].transform(max) == groupby_mercer_set_full_df['title_to_match_count']
groupby_mercer_set_full_df[id_max_df].to_csv(r'\\filer01\DataAnalyticsCoSo\TomM\Mercer Matches\10.12 id_max_df_test1.csv', index=False)
print(len(groupby_mercer_set_full_df[id_max_df]))


#TODO drop duplicates survey_code and survey title duplicates keeping first instance (DONE ABOVE)
groupby_mercer_set_full_df = groupby_mercer_set_full_df.drop_duplicates(subset=['survey_job_title', 'survey_job_survey_code'], take_last=False)
groupby_mercer_set_full_df = groupby_mercer_set_full_df.add_suffix('_count').reset_index()
print(groupby_mercer_set_full_df.columns)

#TODO create the 4 groupbys then merge together on 'survey_job_title' 'and 'survey_code' (DONE)
#TODO the other thing we could do is combine survey_job, org_job, ee_job and payscale job into one column match those and (DONE)
#TODO find counts dropping duplicates (DONE)